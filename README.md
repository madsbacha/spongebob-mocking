# spongebob

An easy way to convert text to [spongebob mocking meme text](https://knowyourmeme.com/memes/mocking-spongebob). You can try a live version [here](https://spongebob.madsbachandersen.dk).

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
